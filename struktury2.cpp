#include <iostream>
#include <fstream>

using namespace std;

typedef unsigned int uint;
typedef unsigned char uchar;

struct Osoba {
    string imie,nazwisko;
    //bool kobieta;
    uchar plec;
    uint id_miasto;
    string ulica;
    string nr_domu,nr_mieszkania;
    string data_urodzenia;
    string pesel;
};

struct Miasto {
    string miasto,kod,opis;
};


///należy zaimplementować
void odczytaj_miasta(string nazwa, Miasto *m);
void zapisz_miasta(string nazwa, Miasto *m, int max);
void odczytaj_osoby(string nazwa, Osoba *o);
void zapisz_osoby(string nazwa, Osoba *o, int max);
Miasto pobierz_miasto(int id, Miasto *m);
Osoba pobierz_osobe(int id, Osoba *o);
bool usun_miasto(int id, Miasto *m, int max);
bool usun_osobe(int id, Osoba *o, int max);
bool zamien_miasto(Miasto m, int id, Miasto *mp);
bool zamien_osobe(Osoba o, int id, Osoba *op);

//przetestowac na 20 wpisach!

int menu();
string get_nazwa() {string r; cout << "\nPodaj nazwe pliku: "; cin >> r; return r;}

int main()
{

    int max_miasta=30,max_osoby=100,
            mp=0,op=0;
    Miasto miasta[max_miasta];
//    Miasto *wsk_miasta = new Miasto[max_miasta];
    Osoba osoby[max_osoby];
    while (1) {
        switch (menu()) {
        case 1: odczytaj_miasta(get_nazwa(), miasta);
            break;
        case 2: odczytaj_osoby(get_nazwa(), osoby); break;
        case 3: zapisz_miasta(get_nazwa(), miasta, max_miasta);break;
        case 4: zapisz_osoby(get_nazwa(), osoby, max_osoby);break;
        default:
            return 0;
        }
    }

    return 0;
}

int menu() {
    int opcja = 0;
    cout << "Wybierz jedna z opcji:\n1 - Wczytaj miasta z pliku\n2 - Wszytaj osoby z pliku\n3 - Zapisz miasta do pliku"
         << "\n4 - Zapisz osoby do pliku\n5 - Wyswietl miasta\n6 - Wyswietl osoby\n7 - Usun miasto\n8 - Usun osobe"
         << "\n9 - Zmien miasto\n10 - Zmien osobe\n11 - Koniec programu\nTwoj wybor: ";
    cin >> opcja;
    return opcja;
}

void odczytaj_miasta(string nazwa, Miasto *m) {
    ifstream plik(nazwa);
    int p=0;
    if (!plik.is_open()) return;
    while(!plik.eof()) {
        plik >> m[p].miasto >> m[p].kod /*>> m[p].opis*/;
        p++;
    }
    plik.close();
}

void zapisz_miasta(string nazwa, Miasto *m, int max) {
    ofstream plik(nazwa);
    if (!plik.is_open()) return;
    for (int i =0;i<max;i++)
        plik << m[i].miasto << ' ' << m[i].kod << '\n';
    plik.close();
}

void odczytaj_osoby(string nazwa, Osoba *o) {
    ifstream plik(nazwa);
    int p=0;
    if (!plik.is_open()) return;
    while (!plik.eof()) {
        plik >> o[p].imie >> o[p].nazwisko >> o[p].pesel;
        p++;
    }
    plik.close();
}

void zapisz_osoby(string nazwa, Osoba *o, int max) {
    ofstream plik(nazwa);
    if (!plik.is_open()) return;
    for (int i =0;i<max;i++)
        plik << o[i].imie << ' ' << o[i].nazwisko << ' ' << o[i].pesel << '\n';
    plik.close();
}

Miasto pobierz_miasto(int id, Miasto *m) {
    return m[id];
}

Osoba pobierz_osobe(int id, Osoba *o) {
    return o[id];
}

bool usun_miasto(int id, Miasto *m, int max) {
    for (int i=id+1;i<max;i++)
        m[i-1]=m[i];
    if (id<max-1) {
        m[max-1].miasto="";
        m[max-1].kod="";
        return true;
    }
    return false;
}

bool usun_osobe(int id, Osoba *o, int max) {
    for (int i=id+1;i<max;i++)
        o[i-1]=o[i];
    if (id<max-1) {
        o[max-1].imie="";
        o[max-1].nazwisko="";
        o[max-1].pesel="";
        return true;
    }
    return false;
}

bool zamien_miasto(Miasto m, int id, Miasto *mp) {

    mp[id]=m;
    return true;
}

bool zamien_osobe(Osoba o, int id, Osoba *op) {
    op[id]=o;
    return true;
}
